<?php

namespace Test;

use GetRepo\FormYaml\ConfigurableEntityType;
use GetRepo\FormYaml\Exception\RuntimeException;
use GetRepo\FormYaml\Resolver\Resolver;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\ChoiceList\Loader\CallbackChoiceLoader;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Forms;
use Symfony\Component\Form\SubmitButton;
use Symfony\Component\OptionsResolver\Exception\InvalidOptionsException;
use Symfony\Component\OptionsResolver\Exception\MissingOptionsException;
use Symfony\Component\OptionsResolver\Exception\UndefinedOptionsException;
use Symfony\Component\Validator\Constraints\AtLeastOneOf;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Yaml\Exception\ParseException;

class ConfigurableEntityTypeTest extends TestCase
{
    public function optionsFailedData(): array
    {
        return [
            'Required option' => [
                MissingOptionsException::class,
                'The required options "data", "form_definitions" are missing.',
            ],
            'Option "form_definitions" is missing' => [
                MissingOptionsException::class,
                'The required option "form_definitions" is missing.',
                'data_class', // data
            ],
            'Option whatever' => [
                UndefinedOptionsException::class,
                'The option "whatever" does not exist',
                'data_class',
                ['whatever' => 1],
            ],
            'Data class does not exists' => [
                UndefinedOptionsException::class,
                "Data class 'data_class' does not exists.",
                'data_class',
                ['form_definitions' => 1],
            ],
            'Option form_definitions wrong type' => [
                InvalidOptionsException::class,
                'The option "form_definitions" with value 1 is expected to be of type "array"',
                self::class, // random class name
                ['form_definitions' => 1],
            ],
            'Yaml file does not exists' => [
                ParseException::class,
                'File "/file/path/does/not/exists.yaml" does not exist.',
                EntityTest::class,
                [
                    'form_definitions' => '/file/path/does/not/exists.yaml',
                ],
            ],
            'Invalid callback' => [
                RuntimeException::class,
                "Callback 'NOT_VALID' could not be resolved.",
                EntityTest::class,
                [
                    'form_definitions' => [
                        'name' => [
                            'events' => [
                                'post_set_data' => [
                                    'callback' => 'NOT_VALID',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'Constraint could not be resolved' => [
                RuntimeException::class,
                'Constraint class "DoesNotExists" could not be resolved.',
                EntityTest::class,
                ['form_definitions' => [
                    'name' => [
                        'options' => ['constraints' => 'DoesNotExists'],
                    ],
                ]],
            ],
        ];
    }

    /**
     * @dataProvider optionsFailedData
     */
    public function testOptionsFailed(string $exception, string $message, mixed $data = null, array $options = []): void
    {
        $this->expectException($exception); // @phpstan-ignore-line
        $this->expectExceptionMessage($message);
        $this->getConfigurableEntityFormType($data, $options);
    }

    public function optionsValidData(): array
    {
        $submit = function (FormInterface $form): void {
            $options = $form->getConfig()->getOptions();
            $this->assertInstanceOf(SubmitButton::class, $form);
            $this->assertEquals('Submit', $options['label']);
        };

        $path = sys_get_temp_dir() . '/formyaml_' . __FUNCTION__ . '.yml';
        file_put_contents($path, "name: ~");

        return [
            'Definitions as yaml file path' => [
                [
                    'name' => function (FormInterface $form): void {
                        $this->assertInstanceOf(Form::class, $form);
                    },
                    '_submit' => $submit,
                ],
                ['form_definitions' => $path], // form_definitions as YAML file test
            ],
            'Empty form definitions' => [
                [
                    '_submit' => $submit,
                ],
                ['form_definitions' => []],
            ],
            'Custom submit label' => [
                [
                    '_submit' => function (FormInterface $form): void {
                        $options = $form->getConfig()->getOptions();
                        $this->assertInstanceOf(SubmitButton::class, $form);
                        $this->assertEquals('custom', $options['label']);
                    },
                ],
                ['form_definitions' => [], 'submit_label' => 'custom'],
            ],
            'Custom submit attr' => [
                [
                    '_submit' => function (FormInterface $form): void {
                        $options = $form->getConfig()->getOptions();
                        $this->assertInstanceOf(SubmitButton::class, $form);
                        $this->assertEquals(['class' => 'test'], $options['attr']);
                    },
                ],
                ['form_definitions' => [], 'submit_attr' => ['class' => 'test']],
            ],
            'One form field' => [
                $twoFields = [
                    'name' => function (FormInterface $form): void {
                        $this->assertInstanceOf(Form::class, $form);
                        $this->assertEmpty($form->all());
                    },
                    '_submit' => $submit,
                ],
                ['form_definitions' => [
                    'name' => [],
                ]],
            ],
            'One disabled form field' => [
                [
                    '_submit' => $submit,
                ],
                ['form_definitions' => ['test' => ['enabled' => false]]], // form_definitions as YAML file test
            ],
            'form field default text' => [
                [
                    'name' => function (FormInterface $form): void {
                        $this->assertInstanceOf(
                            TextType::class,
                            $form->getConfig()->getType()->getInnerType()
                        );
                    },
                    '_submit' => $submit,
                ],
                ['form_definitions' => ['name' => []]],
            ],
            'Specific form type class' => [
                [
                    'name' => function (FormInterface $form): void {
                        $this->assertInstanceOf(
                            LanguageType::class,
                            $form->getConfig()->getType()->getInnerType()
                        );
                    },
                    '_submit' => $submit,
                ],
                ['form_definitions' => [
                    'name' => [
                        'type' => LanguageType::class,
                    ],
                ]],
            ],
            'Specific form type RepeatedType, type as string' => [
                [
                    'name' => function (FormInterface $form): void {
                        $this->assertInstanceOf(
                            RepeatedType::class,
                            $form->getConfig()->getType()->getInnerType()
                        );
                    },
                    '_submit' => $submit,
                ],
                ['form_definitions' => [
                    'name' => [
                        'type' => RepeatedType::class,
                        'options' => ['type' => 'LanguageType'],
                    ],
                ]],
            ],
            'Specific form type ChoiceType, choice_loader callback' => [
                [
                    'name' => function (FormInterface $form): void {
                        $this->assertInstanceOf(
                            ChoiceType::class,
                            $form->getConfig()->getType()->getInnerType()
                        );
                        $options = $form->getConfig()->getOptions();
                        $this->assertInstanceOf(CallbackChoiceLoader::class, $options['choice_loader']);
                        /** @var CallbackChoiceLoader $choiceLoader */
                        $choiceLoader = $options['choice_loader'];
                        $this->assertEquals([1, 2, 3], $choiceLoader->loadChoiceList()->getValues());
                    },
                    '_submit' => $submit,
                ],
                ['form_definitions' => [
                    'name' => [
                        'type' => ChoiceType::class,
                        'options' => ['choice_loader' => [EntityTest::class, 'getChoices']],
                    ],
                ]],
            ],
            'Constraints as array of objects' => [
                $constraintAssert = [
                    'name' => function (FormInterface $form): void {
                        $options = $form->getConfig()->getOptions();
                        $this->assertArrayHasKey('constraints', $options);
                        $this->assertCount(1, $options['constraints']);
                        $this->assertInstanceOf(NotBlank::class, $options['constraints'][0]);
                    },
                    '_submit' => $submit,
                ],
                ['form_definitions' => [
                    'name' => [
                        'options' => ['constraints' => [new NotBlank()]],
                    ],
                ]],
            ],
            'Constraints as string' => [
                $constraintAssert,
                ['form_definitions' => [
                    'name' => [
                        'options' => ['constraints' => 'NotBlank'],
                    ],
                ]],
            ],
            'Constraints as array of string' => [
                $constraintAssert,
                ['form_definitions' => [
                    'name' => [
                        'options' => ['constraints' => ['NotBlank' => null]],
                    ],
                ]],
            ],
            'Constraints as array with options' => [
                $constraintAssert,
                ['form_definitions' => [
                    'name' => [
                        'options' => ['constraints' => ['NotBlank' => ['message' => 'error']]],
                    ],
                ]],
            ],
            'Constraints as array of array constraint' => [
                $constraintAssert,
                ['form_definitions' => [
                    'name' => [
                        'options' => ['constraints' => [['NotBlank' => null]]],
                    ],
                ]],
            ],
            'Specific assert choice callback as array' => [
                [
                    'name' => function (FormInterface $form): void {
                        $options = $form->getConfig()->getOptions();
                        $this->assertArrayHasKey('constraints', $options);
                        $this->assertCount(1, $options['constraints']);
                        $this->assertInstanceOf(Choice::class, $options['constraints'][0]);
                    },
                    '_submit' => $submit,
                ],
                ['form_definitions' => [
                    'name' => [
                        'options' => ['constraints' => [['Choice' => [
                            'callback' => [EntityTest::class, 'getChoices'],
                        ]]]],
                    ],
                ]],
            ],
            'Specific assert choice callback from container' => [
                [
                    'name' => function (FormInterface $form): void {
                        $options = $form->getConfig()->getOptions();
                        $this->assertArrayHasKey('constraints', $options);
                        $this->assertCount(1, $options['constraints']);
                        $this->assertInstanceOf(Choice::class, $options['constraints'][0]);
                        /** @var Choice $choice */
                        $choice = $options['constraints'][0];
                        $this->assertNull($choice->callback);
                        $this->assertIsArray($choice->choices);
                        $this->assertCount(8, $choice->choices);
                    },
                    '_submit' => $submit,
                ],
                ['form_definitions' => [
                    'name' => [
                        'options' => ['constraints' => [[
                            'Choice' => ['callback' => ['@this_test_as_service', 'optionsFailedData']],
                        ]]],
                    ],
                ]],
            ],
            'Specific assert composite' => [
                [
                    'name' => function (FormInterface $form): void {
                        $options = $form->getConfig()->getOptions();
                        $this->assertArrayHasKey('constraints', $options);
                        $this->assertCount(1, $options['constraints']);
                        $this->assertInstanceOf(AtLeastOneOf::class, $options['constraints'][0]);
                        $constraints = $options['constraints'][0]->getNestedConstraints();
                        $this->assertCount(2, $constraints);
                        $this->assertInstanceOf(Url::class, $constraints[0]);
                        $this->assertInstanceOf(Regex::class, $constraints[1]);
                    },
                    '_submit' => $submit,
                ],
                ['form_definitions' => [
                    'name' => [
                        'options' => ['constraints' => [
                            'AtLeastOneOf' => [
                                'Url' => null,
                                'Regex' => '/^blabla/',
                            ],
                        ]],
                    ],
                ]],
            ],
            'Field form_name condition = false for name field' => [
                ['_submit' => $submit],
                [
                    'form_definitions' => [
                        'name' => [
                            'condition' => 'form_name == "WRONG"',
                        ],
                    ],
                ],
            ],
            'Field form_name condition = true for name field' => [
                $twoFields,
                [
                    'form_definitions' => [
                        'name' => [
                            'condition' => 'form_name == "name"',
                        ],
                    ],
                ],
            ],
            'Field form_options condition = false for name field' => [
                [
                    '_submit' => $submit,
                ],
                [
                    'form_definitions' => [
                        'name' => [
                            'condition' => 'form_options["help"] == "WRONG"',
                        ],
                    ],
                ],
            ],
            'Field form_options condition = true for name field' => [
                $twoFields,
                [
                    'form_definitions' => [
                        'name' => [
                            'condition' => 'form_options["help"] == NULL',
                        ],
                    ],
                ],
            ],
            'Specific expression_language_values condition = false for name field' => [
                [
                    '_submit' => $submit,
                ],
                [
                    'form_definitions' => [
                        'name' => [
                            'condition' => 'custom > 1000',
                        ],
                    ],
                    'expression_language_values' => [
                        'custom' => 100,
                    ],
                ],
            ],
            'Specific expression_language_values condition = true for name field' => [
                $twoFields,
                [
                    'form_definitions' => [
                        'name' => [
                            'condition' => 'custom > 5',
                        ],
                    ],
                    'expression_language_values' => [
                        'custom' => 100,
                    ],
                ],
            ],
            'priority ordering' => [
                [
                    'name' => function (FormInterface $form): void {
                        $options = $form->getConfig()->getOptions();
                        $this->assertArrayHasKey('priority', $options);
                        $this->assertEquals(1, $options['priority']);
                    },
                    'title' => function (FormInterface $form): void {
                        $options = $form->getConfig()->getOptions();
                        $this->assertArrayHasKey('priority', $options);
                        $this->assertEquals(500, $options['priority']);
                    },
                    '_submit' => $submit,
                ],
                [
                    'form_definitions' => [
                        'name' => [],
                        'title' => ['options' => ['priority' => 500]],
                    ],
                ],
            ],
            'transformData and reverseTransformData entity callbacks events for title field' => [
                [
                    'title' => function (FormInterface $form): void {
                        $transformers = $form->getConfig()->getModelTransformers();
                        $this->assertIsArray($transformers);
                        $this->assertCount(1, $transformers);
                        $this->assertInstanceOf(CallbackTransformer::class, $transformers[0]);
                        $transformer = $transformers[0];
                        $this->assertEquals(
                            'test1 - Test\EntityTest::transformDataTitle',
                            $transformer->transform('test1')
                        );
                        $this->assertEquals(
                            'test2 - Test\EntityTest::reverseTransformDataTitle',
                            $transformer->reverseTransform('test2')
                        );
                    },
                    '_submit' => $submit,
                ],
                [
                    'form_definitions' => [
                        'title' => [],
                    ],
                ],
            ],
            'transformData and reverseTransformData entity callbacks events for name field' => [
                [
                    'name' => function (FormInterface $form): void {
                        $listeners = $form->getConfig()->getEventDispatcher()->getListeners();
                        $this->assertArrayHasKey('form.post_set_data', $listeners);
                        /** @var array<callable> $postSetData */
                        $postSetData = $listeners['form.post_set_data'];
                        $this->assertIsCallable($postSetData[0]);
                        $this->assertEquals('called', ($postSetData[0])());
                    },
                    '_submit' => $submit,
                ],
                [
                    'form_definitions' => [
                        'name' => [
                            'events' => [
                                'post_set_data' => [
                                    'callback' => fn (): string => 'called',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'transformData and reverseTransformData entity callbacks events for name field as string' => [
                [
                    'name' => function (FormInterface $form): void {
                        $listeners = $form->getConfig()->getEventDispatcher()->getListeners();
                        $this->assertArrayHasKey('form.post_set_data', $listeners);
                        /** @var array<callable> $postSetData */
                        $postSetData = $listeners['form.post_set_data'];
                        $this->assertIsCallable($postSetData[0]);
                    },
                    '_submit' => $submit,
                ],
                [
                    'form_definitions' => [
                        'name' => [
                            'events' => [
                                'post_set_data' => 'getChoices',
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @dataProvider optionsValidData
     */
    public function testOptionsValid(array $expected, array $options): void
    {
        $containerMock = $this->createMock(ContainerInterface::class);
        $containerMock->method('has')
            ->with($this->equalTo('this_test_as_service'))
            ->willReturn(true);
        $containerMock->method('get')
            ->with($this->equalTo('this_test_as_service'))
            ->willReturn($this);

        $options['resolver'] = new Resolver(container: $containerMock);
        $form = $this->getConfigurableEntityFormType(EntityTest::class, $options);
        $this->assertInstanceOf(Form::class, $form);
        $fields = $form->all();
        $this->assertCount(count($expected), $fields);
        foreach ($expected as $expectedName => $callback) {
            $actualName = \key($fields);
            $actualField = \current($fields);
            $this->assertEquals($expectedName, $actualName);
            if (\is_callable($callback)) {
                $callback($actualField);
            }
            \next($fields);
        }
    }

    private function getConfigurableEntityFormType(mixed $data, array $options = []): FormInterface
    {
        $type = new ConfigurableEntityType();
        $validator = Validation::createValidator();

        return Forms::createFormFactoryBuilder()
            ->addType($type)
            ->addExtension(new ValidatorExtension($validator))
            ->getFormFactory()
            ->create(ConfigurableEntityType::class, $data, $options);
    }
}
