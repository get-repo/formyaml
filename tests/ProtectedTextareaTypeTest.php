<?php

namespace Test;

use GetRepo\FormYaml\Type\ProtectedTextareaType;

class ProtectedTextareaTypeTest extends ProtectedTextTypeTest
{
    protected static function typeClass(): string
    {
        return ProtectedTextareaType::class;
    }
}
