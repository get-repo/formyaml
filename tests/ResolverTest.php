<?php

namespace Test;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Andx;
use Doctrine\ORM\QueryBuilder;
use GetRepo\FormYaml\Resolver\Resolver;
use GetRepo\FormYaml\Type\FontAwesomeIconType;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Validator\Constraints\Choice;
use Symfony\Component\Validator\Constraints\GreaterThan;
use Symfony\Component\Validator\Constraints\Json;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\NotNull;
use Symfony\Component\Validator\Constraints\Sequentially;
use Symfony\Component\Validator\Constraints\When;
use Symfony\Component\Validator\Constraints\WhenValidator;

class ResolverTest extends TestCase
{
    private static Resolver $resolver;

    public static function setUpBeforeClass(): void
    {
        self::$resolver = new Resolver();
    }

    public function isFormClassData(): array
    {
        return [
            // failures
            'Form class does not exists' => ['form_class_does_not_exists', false],
            'Form class does not implement FormTypeInterface' => [self::class, false],
            // success
            'Form class exists' => [SubmitType::class],
        ];
    }

    /**
     * @dataProvider isFormClassData
     */
    public function testIsFormClass(string $formClass, bool $expected = true): void
    {
        $this->assertEquals($expected, self::$resolver->isFormClass($formClass));
    }

    public function resolveFormClassData(): array
    {
        return [
            // failures
            'Form class does not exists' => ['form_class_does_not_exists'],
            'Form class does not exists full name' => [self::class],
            // success
            'Form class full name' => [SubmitType::class, SubmitType::class],
            'Form class short name with type' => ['SubmitType', SubmitType::class],
            'Form class short name with type lower underscore' => ['submit_type', SubmitType::class],
            'Form class short name with type lower spaces' => ['submit type', SubmitType::class],
            'Form class short name without type' => ['Submit', SubmitType::class],
            'Form class short name without type lower' => ['submit', SubmitType::class],
            'Form class with spaces (trimmed)' => ['    SubmitType        ', SubmitType::class],
        ];
    }

    /**
     * @dataProvider resolveFormClassData
     */
    public function testResolveFormClass(string $formClass, string $expected = null): void
    {
        if (!$expected) {
            $this->expectExceptionMessage(sprintf('Form class "%s" could not be resolved', $formClass));
        }
        $this->assertEquals($expected, self::$resolver->resolveFormClass($formClass));
    }

    public function testResolveFormClassWithDefault(): void
    {
        $this->assertEquals('OK', self::$resolver->resolveFormClass('form_class_does_not_exists', ['default' => 'OK']));
    }

    public function testResolveFormClassWithSuffix(): void
    {
        $this->assertEquals(
            FontAwesomeIconType::class,
            self::$resolver->resolveFormClass('FontAwesome', ['suffix' => 'Icon']),
        );
    }

    public function testResolveFormClassWithDisableSymfonyNamespace(): void
    {
        $this->expectExceptionMessage('Form class "Submit" could not be resolved.');
        $this->assertEquals(
            SubmitType::class,
            self::$resolver->resolveFormClass('Submit', ['disable_symfony_namespace' => true]),
        );
    }

    public function resolveFormOptionsData(): array
    {
        return [
            // failures
            'Form class does not exists' => [
                'form_class_does_not_exists',
                [],
                'Form class "form_class_does_not_exists" could not be resolved',
            ],
            'Entry type class does not exists' => [
                TextType::class,
                ['entry_type' => 'entry_type_does_not_exists'],
                'Form class "entry_type_does_not_exists" could not be resolved',
            ],
            'Repeated type invalid type' => [
                'RepeatedType',
                ['type' => 'invalid'],
                'Form class "invalid" could not be resolved',
            ],
            // success
            'Entry type resolved' => [
                TextType::class,
                ['entry_type' => 'Submit'],
                ['entry_type' => SubmitType::class],
            ],
            'Checkbox type required' => [
                'checkbox',
                [],
                ['required' => false],
            ],
            'Repeated type required' => [
                'RepeatedType',
                ['type' => 'Checkbox'],
                ['type' => CheckboxType::class],
            ],
            'CollectionType entry options' => [
                'CollectionType',
                ['entry_options' => ['test' => true]],
                ['entry_options' => ['test' => true]],
            ],
            'Choice choice_attr callback resolved' => [
                'Choice',
                ['choice_attr' => [EntityTest::class, 'getAttr']],
                ['choice_attr' => \Closure::fromCallable([EntityTest::class, 'getAttr'])],
            ],
            'Choice choice_attr callback unresolved' => [
                'Choice',
                ['choice_attr' => ['class' => 5]],
                ['choice_attr' => ['class' => 5]],
            ],
        ];
    }

    /**
     * @dataProvider resolveFormOptionsData
     */
    public function testResolveFormOptions(string $formClass, array $options, array|string $expected): void
    {
        if (is_string($expected)) {
            $this->expectExceptionMessage($expected);
        }
        $this->assertEquals($expected, self::$resolver->resolveFormOptions($formClass, $options));
    }

    public function isConstraintClassData(): array
    {
        return [
            // failures
            'Constraint class does not exists' => ['constraint_class_does_not_exists', false],
            'Constraint class does not extends abstract Constraint' => [self::class, false],
            // success
            'Constraint class exists (Length)' => [Length::class],
            'Constraint class exists (GreaterThan extends another constraint)' => [GreaterThan::class],
        ];
    }

    /**
     * @dataProvider isConstraintClassData
     */
    public function testIsConstraintClass(string $constraintClass, bool $expected = true): void
    {
        $this->assertEquals($expected, self::$resolver->isConstraintClass($constraintClass));
    }

    public function resolveConstraintClassData(): array
    {
        return [
            // failures
            'Constraint class does not exists' => ['constraint_does_not_exists'],
            'Constraint class does not extends abstract Constraint' => [self::class],
            'Constraint class composite' => [WhenValidator::class],
            // success
            'Constraint class full name' => [NotNull::class, NotNull::class],
            'Constraint class short name' => ['NotNull', NotNull::class],
            'Constraint class short name lower underscore' => ['not_null', NotNull::class],
            'Constraint class short name lower spaces' => ['not null', NotNull::class],
            'Constraint class with spaces (trimmed)' => ['    NotNull        ', NotNull::class],
        ];
    }

    /**
     * @dataProvider resolveConstraintClassData
     */
    public function testResolveConstraintClass(string $constraintClass, string $expected = null): void
    {
        if (!$expected) {
            $this->expectExceptionMessage(sprintf('Constraint class "%s" could not be resolved', $constraintClass));
        }
        $this->assertEquals($expected, self::$resolver->resolveConstraintClass($constraintClass));
    }

    public function testResolveConstraintClassWithoutThrow(): void
    {
        $this->assertNull(self::$resolver->resolveConstraintClass('constraint_does_not_exists', false));
    }

    public function resolveConstraintsData(): array
    {
        return [
            'simple constraint' => [
                ['NotBlank' => null],
                function (mixed $constraints) {
                    $this->assertIsArray($constraints);
                    $this->assertCount(1, $constraints);
                    $this->assertInstanceOf(NotBlank::class, $constraints[0]);
                }
            ],
            'simple constraints' => [
                ['NotBlank' => null, 'NotNull' => null],
                function (mixed $constraints) {
                    $this->assertIsArray($constraints);
                    $this->assertCount(2, $constraints);
                    $this->assertInstanceOf(NotBlank::class, $constraints[0]);
                    $this->assertInstanceOf(NotNull::class, $constraints[1]);
                }
            ],
            'Composite when constraint' => [
                [
                    [
                        'When' => [
                            'expression' => '1 == 1',
                            'constraints' => [
                                ['Json' => ['message' => 'test']],
                                ['NotBlank' => null],
                            ],
                        ],
                    ],
                ],
                function (mixed $constraints) {
                    $this->assertIsArray($constraints);
                    $this->assertCount(1, $constraints);
                    $when = current($constraints);
                    $this->assertInstanceOf(When::class, $when);
                    $this->assertCount(2, $when->constraints);
                    $this->assertInstanceOf(Json::class, $when->constraints[0]);
                    $this->assertEquals('test', $when->constraints[0]->message);
                    $this->assertInstanceOf(NotBlank::class, $when->constraints[1]);
                    $this->assertEquals('1 == 1', $when->expression);
                }
            ],
            'Simple and composite sequentially constraint' => [
                [
                    'NotBlank' => null,
                    'Sequentially' => [
                        'NotBlank' => null,
                        'Length' => ['min' => 10],
                    ],
                ],
                function (mixed $constraints) {
                    $this->assertIsArray($constraints);
                    $this->assertCount(2, $constraints);
                    $this->assertInstanceOf(NotBlank::class, $constraints[0]);
                    $this->assertInstanceOf(Sequentially::class, $constraints[1]);
                    $sequentially = $constraints[1];
                    $this->assertCount(2, $sequentially->constraints);
                    $this->assertInstanceOf(NotBlank::class, $sequentially->constraints[0]);
                    $this->assertInstanceOf(Length::class, $sequentially->constraints[1]);
                    $this->assertEquals(10, $sequentially->constraints[1]->min);
                }
            ],
        ];
    }

    /**
     * @dataProvider resolveConstraintsData
     */
    public function testResolveConstraints(
        array $constraints,
        callable $expects,
    ): void {
        $constraints = self::$resolver->resolveConstraints($constraints);
        $expects($constraints);
    }

    public function resolveConstraintData(): array
    {
        return [
            // failures
            'Constraint class does not exists' => [
                'constraint_does_not_exists',
                [],
                'Constraint class "constraint_does_not_exists" could not be resolved.',
            ],
            // success
            'Constraint NotNull simple without options' => [
                'NotNull',
                [],
                function (object $constraint) {
                    $this->assertInstanceOf(NotNull::class, $constraint, 'not null class constraint');
                },
            ],
            'Constraint length as main arg' => [
                'length',
                [6],
                $lengthAssert = function (object $constraint) {
                    $this->assertInstanceOf(Length::class, $constraint, 'length class constraint');
                    /** @var Length $constraint */
                    $this->assertEquals(6, $constraint->max, 'length max');
                    $this->assertEquals(6, $constraint->min, 'length min');
                },
            ],
            'Constraint length as options' => [
                'length',
                ['min' => 6, 'max' => 6],
                $lengthAssert,
            ],
            'Constraint choice with callback' => [
                Choice::class,
                ['callback' => [EntityTest::class, 'getChoices']],
                function (object $constraint) {
                    $this->assertInstanceOf(Choice::class, $constraint, 'choice class constraint');
                    /** @var Choice $constraint */
                    $this->assertNull($constraint->callback);
                    $this->assertEquals([1, 2, 3], $constraint->choices);
                },
            ],
        ];
    }

    /**
     * @dataProvider resolveConstraintData
     */
    public function testResolveConstraint(
        string $constraintClass,
        array $options = [],
        string|callable $expected = null,
    ): void {
        if (is_string($expected)) {
            $this->expectExceptionMessage($expected);
        }
        $constraint = self::$resolver->resolveConstraint($constraintClass, $options);
        $this->assertIsObject($constraint);
        if (is_callable($expected)) {
            $expected($constraint);
        }
    }

    public function resolveCallBackData(): array
    {
        return [
            // failures
            'callback invalid' => ['callback_invalid', 'Callback \'callback_invalid\' could not be resolved'],
            // success
            'callback sys_get_temp_dir()' => ['sys_get_temp_dir'],
            'callback array' => [[$this, 'resolveCallBackData']],
        ];
    }

    /**
     * @dataProvider resolveCallBackData
     */
    public function testResolveCallBack(mixed $callback, string $exception = null): void
    {
        if ($exception) {
            $this->expectExceptionMessage($exception);
        }
        $closure = self::$resolver->resolveCallBack($callback);
        $this->assertInstanceOf(\Closure::class, $closure);
        $closure(); // closure must be callable without arguments
    }



    public function resolveQueryBuilderData(): array
    {
        $repoMock = $this->createMock(EntityRepository::class);
        $repoMock->method('createQueryBuilder')
            ->willReturn(new QueryBuilder($this->createMock(EntityManagerInterface::class)));

        return [
            'Empty query builder' => [
                [],
                [],
                function (\Closure $closure) use ($repoMock): void {
                    /** @var QueryBuilder $qb */
                    $qb = $closure($repoMock);
                    $this->assertInstanceOf(QueryBuilder::class, $qb);
                    // nothing generated
                    $this->assertEmpty(array_filter($qb->getDQLParts()));
                },
            ],
            'Query builder where' => [
                ['where' => 'where condition'],
                [],
                function (\Closure $closure) use ($repoMock): void {
                    /** @var QueryBuilder $qb */
                    $qb = $closure($repoMock);
                    $this->assertInstanceOf(QueryBuilder::class, $qb);
                    // nothing generated
                    $dqlParts = array_filter($qb->getDQLParts());
                    $this->assertNotEmpty($dqlParts);
                    $this->assertArrayHasKey('where', $dqlParts);
                    $this->assertInstanceOf(Andx::class, $dqlParts['where']);
                    /** @var Andx $andX */
                    $andX = $dqlParts['where'];
                    $this->assertEquals(['where condition'], $andX->getParts());
                },
            ],
        ];
    }

    /**
     * @dataProvider resolveQueryBuilderData
     */
    public function testResolveQueryBuilder(array $queryBuilder, array $options, callable $expected): void
    {
        $closure = self::$resolver->resolveQueryBuilder($queryBuilder, $options);
        $this->assertIsCallable($closure);
        $expected($closure);
    }
}
