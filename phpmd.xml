<?xml version="1.0"?>

<ruleset name="Unused Code Rules"
         xmlns="http://pmd.sf.net/ruleset/1.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://pmd.sf.net/ruleset/1.0.0 http://pmd.sf.net/ruleset_xml_schema.xsd"
         xsi:noNamespaceSchemaLocation="http://pmd.sf.net/ruleset_xml_schema.xsd">
    <description>
The Unused Code Ruleset contains a collection of rules that find unused code.
    </description>

    <rule name="UnusedLocalVariable"
          since="0.2"
          message="Avoid unused local variables such as '{0}'."
          class="PHPMD\Rule\UnusedLocalVariable"
          externalInfoUrl="https://phpmd.org/rules/unusedcode.html#unusedlocalvariable">
        <description>
Detects when a local variable is declared and/or assigned, but not used.
        </description>
        <priority>3</priority>
        <properties>
            <property
                    name="allow-unused-foreach-variables"
                    description="Allow unused variables in foreach language constructs."
                    value="false" />
            <property name="exceptions" description="Comma-separated list of exceptions" value=""/>
        </properties>
    </rule>

    <rule name="ConstantNamingConventions"
          since="0.2"
          message="Constant {0} should be defined in uppercase"
          class="PHPMD\Rule\Naming\ConstantNamingConventions"
          externalInfoUrl="https://phpmd.org/rules/naming.html#constantnamingconventions">
        <description>
Class/Interface constant names should always be defined in uppercase.
        </description>
        <priority>4</priority>
        <properties />
    </rule>

    <rule name="ExitExpression"
          since="0.2"
          message = "The {0} {1}() contains an exit expression."
          class="PHPMD\Rule\Design\ExitExpression"
          externalInfoUrl="https://phpmd.org/rules/design.html#exitexpression">
        <description>
            <![CDATA[
An exit-expression within regular code is untestable and therefore it should
be avoided. Consider to move the exit-expression into some kind of startup
script where an error/exception code is returned to the calling environment.
            ]]>
        </description>
        <priority>1</priority>
        <properties />
    </rule>

    <rule name="EvalExpression"
          since="0.2"
          message = "The {0} {1}() contains an eval expression."
          class="PHPMD\Rule\Design\EvalExpression"
          externalInfoUrl="https://phpmd.org/rules/design.html#evalexpression">
        <description>
            <![CDATA[
An eval-expression is untestable, a security risk and bad practice. Therefore
it should be avoided. Consider to replace the eval-expression with regular
code.
            ]]>
        </description>
        <priority>1</priority>
        <properties />
    </rule>

    <rule name="GotoStatement"
          since="1.1.0"
          message="The {0} {1}() utilizes a goto statement."
          class="PHPMD\Rule\Design\GotoStatement"
          externalInfoUrl="https://phpmd.org/rules/design.html#gotostatement">
        <description>
            <![CDATA[
Goto makes code harder to read and it is nearly impossible to understand the
control flow of an application that uses this language construct. Therefore it
should be avoided. Consider to replace Goto with regular control structures and
separate methods/function, which are easier to read.
            ]]>
        </description>
        <priority>1</priority>
        <properties />
    </rule>

    <rule name="DevelopmentCodeFragment"
          since="2.3.0"
          message="The {0} {1}() calls the typical debug function {2}() which is mostly only used during development."
          class="PHPMD\Rule\Design\DevelopmentCodeFragment"
          externalInfoUrl="https://phpmd.org/rules/design.html#developmentcodefragment">
        <description>
            <![CDATA[
Functions like var_dump(), print_r() etc. are normally only used during development
and therefore such calls in production code are a good indicator that they were
just forgotten.
            ]]>
        </description>
        <priority>2</priority>
        <properties>
            <property name="unwanted-functions" value="var_dump,print_r,debug_zval_dump,debug_print_backtrace" description="Comma separated list of suspect function images." />
            <property name="ignore-namespaces" value="false" description="Ignore namespaces when looking for dev. fragments" />
        </properties>
    </rule>

    <rule name="EmptyCatchBlock"
          since="2.7.0"
          message="Avoid using empty try-catch blocks in {0}."
          class="PHPMD\Rule\Design\EmptyCatchBlock"
          externalInfoUrl="https://phpmd.org/rules/design.html#emptycatchblock">
        <description>
            <![CDATA[
Usually empty try-catch is a bad idea because you are silently swallowing an error condition
and then continuing execution. Occasionally this may be the right thing to do, but often
it's a sign that a developer saw an exception, didn't know what to do about it,
and so used an empty catch to silence the problem.
            ]]>
        </description>
        <priority>2</priority>
        <properties />
    </rule>

    <rule name="CamelCaseClassName"
          since="0.2"
          message = "The class {0} is not named in CamelCase."
          class="PHPMD\Rule\Controversial\CamelCaseClassName"
          externalInfoUrl="#">
        <description>
            <![CDATA[
It is considered best practice to use the CamelCase notation to name classes.
            ]]>
        </description>
        <priority>1</priority>
        <properties />
    </rule>

    <rule name="CamelCasePropertyName"
          since="0.2"
          message = "The property {0} is not named in camelCase."
          class="PHPMD\Rule\Controversial\CamelCasePropertyName"
          externalInfoUrl="#">
        <description>
            <![CDATA[
It is considered best practice to use the camelCase notation to name attributes.
            ]]>
        </description>
        <priority>1</priority>
        <properties>
            <property name="allow-underscore"
                      description="Allow an optional, single underscore at the beginning."
                      value="false" />
            <property name="allow-underscore-test"
                      description="Is it allowed to have underscores in test method names."
                      value="false" />
        </properties>
    </rule>

    <rule name="CamelCaseMethodName"
          since="0.2"
          message = "The method {0} is not named in camelCase."
          class="PHPMD\Rule\Controversial\CamelCaseMethodName"
          externalInfoUrl="#">
        <description>
            <![CDATA[
It is considered best practice to use the camelCase notation to name methods.
            ]]>
        </description>
        <priority>1</priority>
        <properties>
            <property name="allow-underscore"
                      description="Allow an optional, single underscore at the beginning."
                      value="false" />
            <property name="allow-underscore-test"
                      description="Is it allowed to have underscores in test method names."
                      value="false" />
        </properties>
    </rule>

    <rule name="CamelCaseParameterName"
          since="0.2"
          message = "The parameter {0} is not named in camelCase."
          class="PHPMD\Rule\Controversial\CamelCaseParameterName"
          externalInfoUrl="#">
        <description>
            <![CDATA[
It is considered best practice to use the camelCase notation to name parameters.
            ]]>
        </description>
        <priority>1</priority>
        <properties>
            <property name="allow-underscore"
                      description="Allow an optional, single underscore at the beginning."
                      value="false" />
        </properties>
    </rule>

    <rule name="CamelCaseVariableName"
          since="0.2"
          message = "The variable {0} is not named in camelCase."
          class="PHPMD\Rule\Controversial\CamelCaseVariableName"
          externalInfoUrl="#">
        <description>
            <![CDATA[
It is considered best practice to use the camelCase notation to name variables.
            ]]>
        </description>
        <priority>1</priority>
        <properties>
            <property name="allow-underscore"
                      description="Allow an optional, single underscore at the beginning."
                      value="false" />
        </properties>
    </rule>
</ruleset>

